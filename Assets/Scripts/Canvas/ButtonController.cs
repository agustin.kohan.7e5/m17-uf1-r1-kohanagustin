using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void GoToScene(string scene)
    {
        GameManager.Instance.LoadScene(scene);
    }

    public void ExitGame()
    {
        //Solo funcionara en la aplicación construida, no en el editor.
        Application.Quit();

        //Para que funcione en el editor:
        //UnityEditor.EditorApplication.isPlaying = false;
    }
}
