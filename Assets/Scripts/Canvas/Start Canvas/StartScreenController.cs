using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System;


public class StartScreenController : MonoBehaviour
{
    
    [SerializeField]
    InputField _nameInputField;

    [SerializeField]
    Button _startButton;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
       
    }

    public void ShowGamemodePanel()
    {
        transform.Find("GameModePanel").gameObject.SetActive(true);
    }

    public void StartGame(string GameMode)
    {
        //GameManager.Instance.SetGameMode(GameMode);
        GameObject.Find("GameData").GetComponent<GameModeController>().SetGamemode(GameMode);
        if(_nameInputField.text != "")
        {
            SceneManager.LoadScene("GameplayScene_Test");
        }

    }
}
