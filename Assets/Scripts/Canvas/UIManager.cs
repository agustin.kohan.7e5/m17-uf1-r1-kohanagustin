using TMPro;
using UnityEngine;

public class UIManager : MonoBehaviour
{

    private static UIManager instance = null;

    //Game Instance Singleton

    public static UIManager Instance { get { return instance; } }

    void Awake()
    {
        // if the singleton hasn't been initialized yet
        if (instance != null && instance != this) { Destroy(this.gameObject); }

        instance = this;
    }

    GameObject _canvas;
    // Start is called before the first frame update
    public void Start()
    {
        _canvas = GameObject.Find("Canvas");
        SetNameInCanvas();
    }

    // Update is called once per frame
    void Update()
    {
      
    }

    public void ShowCounterText()
    {
        _canvas.transform.Find("Counter").gameObject.SetActive(true);
        _canvas.transform.Find("Counter").GetComponent<TextMeshProUGUI>().text = "Warrior dodged " + GameObject.Find("FireballSpawner").GetComponent<FireballCount>().GetFireballCount() + " Fireballs";
    }
    public void SetNameInCanvas() => _canvas.transform.Find("PlayerNameText").GetComponent<TextMeshProUGUI>().text = GameObject.Find("GameData").GetComponent<PlayerName>().NamePlayer;
    public void ShowGameOverCanvas() => _canvas.transform.Find("DefeatPanel").gameObject.SetActive(true);

    public void ShowVictoryCanvas() => _canvas.transform.Find("WinPanel").gameObject.SetActive(true);

    

}
