using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyAllObjects : MonoBehaviour
{
    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            switch (GameManager.Instance.GetGamemode())
            {
                case GameMode.WarriorMode:
                    GameManager.Instance.gameOver(false);
                    break;
                case GameMode.WizarMode:
                    GameManager.Instance.gameOver(true);
                    break;
                default:
                    break;
            }
        }
        Destroy(collision.gameObject);
    }
}
