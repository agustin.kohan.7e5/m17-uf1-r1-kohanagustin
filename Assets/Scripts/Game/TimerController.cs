using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class TimerController : MonoBehaviour
{
    public float timer;
    private TextMeshProUGUI _timerText;
    // Start is called before the first frame update
    void Start()
    {
        _timerText = GameObject.Find("Canvas").transform.Find("TimerText").GetComponent<TextMeshProUGUI>();
    }

    // Update is called once per frame
    void Update()
    {
        timer -= Time.deltaTime;
        _timerText.text = ((int)timer).ToString();

        if (timer <= 0)
        {
            switch (GameManager.Instance.GetGamemode())
            {
                case GameMode.WarriorMode:
                    GameManager.Instance.gameOver(true);
                    break;
                case GameMode.WizarMode:
                    GameManager.Instance.gameOver(false);
                    break;
                default:
                    break;
            }
             
            gameObject.SetActive(false);
        }
    }
}
