using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameModeController : MonoBehaviour
{
    private string GameMode;
    // Start is called before the first frame update
    void Awake() 
    {
        DontDestroyOnLoad(gameObject);
    }

    public void SetGamemode(string gameMode) => GameMode = gameMode;
    public string GetGamemode() => GameMode;

    // Update is called once per frame
    void Update()
    {
        
    }
}