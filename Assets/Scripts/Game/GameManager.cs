using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEditor.SearchService;


public enum GameState
{
    Playing,
    Victory,
    Defeat,
    Pause,
    Waiting
}

public enum GameMode
{
    WarriorMode,
    WizarMode
}


public class GameManager : MonoBehaviour
{
    private static GameManager instance = null;

    //Game Instance Singleton

    public static GameManager Instance { get { return instance; } }

    void Awake()
    {
        // if the singleton hasn't been initialized yet
        if(instance != null && instance != this) { Destroy(this.gameObject); }

        instance= this;
    }

    private void Start()
    {
        StartGameInGamemode();
    }


    public string _gameModeString;
        /*
    public void SetGameMode(string newGameMode) => _gameModeString = newGameMode;
    public string GetGameMode() => _gameModeString;
        */

    GameObject _player;
    GameObject _fireballSpawner;
    GameMode _gameMode;

    public GameMode GetGamemode() => _gameMode;

    public void StartGameInGamemode()
    {
        string gameModeString = GameObject.Find("GameData").GetComponent<GameModeController>().GetGamemode();
        _player = GameObject.Find("Player");
        _fireballSpawner = GameObject.Find("FireballSpawner");
        if (_player != null)
        {
            switch (gameModeString)
            {
                case "WarriorMode":
                    _gameMode = GameMode.WarriorMode;
                    _player.GetComponent<PlayerMovementController>().enabled = true;
                    _player.GetComponent<PlayerMovementAuto>().enabled = false;
                    _fireballSpawner.GetComponent<FireballSpawn>().enabled = true;
                    _fireballSpawner.GetComponent<SpawnerPlayerController>().enabled = false;
                    break;
                case "WizardMode":
                    _gameMode = GameMode.WizarMode;
                    _player.GetComponent<PlayerMovementController>().enabled = false;
                    _player.GetComponent<PlayerMovementAuto>().enabled = true;
                    _fireballSpawner.GetComponent<FireballSpawn>().enabled = false;
                    _fireballSpawner.GetComponent<SpawnerPlayerController>().enabled = true;
                    break;
            }
        }
    }

    public void LoadScene(string scene)
    {
        SceneManager.LoadScene(scene);
    } 

   public void gameOver(bool victory)
    {
        DestroyAllObjectsWithTag("Fireball");
        GameObject.Find("Timer").GetComponent<TimerController>().enabled =  false;
        _player = GameObject.Find("Player");
        _fireballSpawner = GameObject.Find("FireballSpawner");
        _fireballSpawner.GetComponent<FireballSpawn>().enabled = false;
        _player.GetComponent<PlayerMovementController>().enabled = false;
        _fireballSpawner.GetComponent<SpawnerPlayerController>().enabled = false;
        _player.GetComponent<PlayerMovementAuto>().enabled = false;
        UIManager.Instance.ShowCounterText();
        if (victory)
            UIManager.Instance.ShowVictoryCanvas();
        else
            UIManager.Instance.ShowGameOverCanvas();
          
    }

    void DestroyAllObjectsWithTag(string tag)
    {
        GameObject[] gameObjects = GameObject.FindGameObjectsWithTag(tag);

        for (var i = 0; i < gameObjects.Length; i++)
        {
            Destroy(gameObjects[i]);
        }
    }
}
