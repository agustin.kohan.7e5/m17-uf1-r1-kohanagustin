using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class EventStartGame : MonoBehaviour
{
    [SerializeField] private Button _StartButton = null;
    string _playeName;
    // Start is called before the first frame update
    void Start()
    {
        _StartButton.onClick.AddListener(() => { StartUIPanelGame(); });
    }

    bool isValidName;
    public void StartUIPanelGame()
    {
        isValidName = true;
        _playeName = GameObject.Find("PlayerName").GetComponent<PlayerName>().NamePlayer;

        foreach (char character in _playeName)
            if (character == ' ') isValidName = false;
        
        if (isValidName)
            SceneManager.LoadScene("SampleScene");
    }
}
