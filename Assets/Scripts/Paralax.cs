using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Paralax : MonoBehaviour
{
    float lenght; float startpos;
    public float parallaxEfect;


    // Start is called before the first frame update
    void Start()
    {
        startpos = transform.position.x;
        lenght = GetComponent<SpriteRenderer>().bounds.size.x;
    }

    // Update is called once per frame
    void Update()
    {
        float temp = (Camera.main.transform.position.x * (1 - parallaxEfect));
        float dist = (Camera.main.transform.position.x * parallaxEfect);
        transform.position = new Vector3(startpos + dist, transform.position.y, transform.position.z);

        if (temp > startpos + lenght) startpos += lenght*2;
        else if (temp < startpos - lenght) startpos -= lenght;
    }
}
