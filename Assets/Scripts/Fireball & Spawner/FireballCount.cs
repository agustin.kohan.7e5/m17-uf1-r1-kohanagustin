using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireballCount : MonoBehaviour
{

    private int _fireballCount;

    public void AddCount() => _fireballCount++;

    public int GetFireballCount() => _fireballCount;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
