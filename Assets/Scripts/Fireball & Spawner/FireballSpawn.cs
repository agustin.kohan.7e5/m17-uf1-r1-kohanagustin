using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SpawnerState
{
    Active,
    Waiting,
    Disabled,
}
public class FireballSpawn : MonoBehaviour
{

    [SerializeField]
    private GameObject _fireballPrefab;

    public float _spawnTimeLapse;
    private float _cooldownSpawnerTime;
    private FireballCount _fireballCount;
    

    GameObject _player;

    Animator _wizardAnim;

    private SpawnerState _spawnerState;

    private float _gameplayTime;
    // Start is called before the first frame update
    void Start()
    {
        _fireballCount = GetComponent<FireballCount>();
        _cooldownSpawnerTime = 0f;
        _player = GameObject.Find("Player");
        _gameplayTime = 0f;
        _wizardAnim = GameObject.Find("Wizard").GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (_spawnerState == SpawnerState.Active)
        {
            _wizardAnim.SetTrigger("Atack");
            SpawnNewFireball();
            _fireballCount.AddCount();
            _spawnerState = SpawnerState.Waiting;
        }
        else if (_spawnerState == SpawnerState.Waiting)
        {
            _cooldownSpawnerTime += Time.deltaTime;
            if (_cooldownSpawnerTime >= _spawnTimeLapse)
            {
                _spawnerState = SpawnerState.Active;
                _cooldownSpawnerTime = 0;
            }
        }
        _gameplayTime += Time.deltaTime;
        _spawnTimeLapse = 2 / _gameplayTime;
    }

    private void SpawnNewFireball()
    {
        if (_player != null)
        {
            ChangePosition();
            InstantiateFireball();
        }
    }

    private void InstantiateFireball()
    {
        Instantiate(_fireballPrefab, this.transform.position, Quaternion.identity);
    }

    private void ChangePosition() => transform.position = GenerateRandomPosition();
    
    
    private Vector2 GenerateRandomPosition()
    {
        // Vector2 cameraBorders = Camera.main.ScreenToWorldPoint(new Vector2(Screen.width, Screen.height));
        //_distanceFormBorderScreenToPlatform = (cameraBorders.x - _ground.GetComponent<BoxCollider2D>().size.x) / 2;

        //_player.transform.position.x;

        return new Vector2(Random.Range(_player.transform.position.x - 5, _player.transform.position.x + 5), transform.position.y);
    } 
}
