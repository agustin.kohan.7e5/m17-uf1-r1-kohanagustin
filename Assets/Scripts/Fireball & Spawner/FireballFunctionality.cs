using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireballFunctionality : MonoBehaviour
{
    Animator _anim;
    // Start is called before the first frame update
    void Awake()
    {
        _anim = GetComponent<Animator>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.gameObject.tag == "Player")
        {
            GameObject.Find("Player").GetComponent<Animator>().SetTrigger("Death");
            switch (GameManager.Instance.GetGamemode())
            {
                case GameMode.WarriorMode:
                    GameManager.Instance.gameOver(false);
                    break;
                case GameMode.WizarMode:
                    GameManager.Instance.gameOver(true);
                    break;
                default:
                    break;
            }
        }
        else if (collision.gameObject.tag != "Fireball")
            WaitForExplosionAnimation();
    }


    void WaitForExplosionAnimation() {
        _anim.SetTrigger("Hit");
        Destroy(gameObject.GetComponent<Rigidbody2D>());
    }
    // Update is called once per frame
    void Update()
    {
            
    }
}
