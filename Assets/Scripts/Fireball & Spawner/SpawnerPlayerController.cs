using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class SpawnerPlayerController : MonoBehaviour
{

    [SerializeField]
    private GameObject _fireballPrefab;

    private SpawnerState _spawnerState;
    public float _spawnTimeLapse = 2.5f;
    private float _cooldownSpawnerTime;
    private FireballCount _fireballCount;


    Animator _wizardAnim;

    // Start is called before the first frame update
    void Start()
    {
        _fireballCount = GetComponent<FireballCount>();
        _cooldownSpawnerTime = 0f;  
        _wizardAnim = GameObject.Find("Wizard").GetComponent<Animator>();
    }

    

    // Update is called once per frame
    void Update()
    {
        if(Input.GetMouseButtonDown(0) && _spawnerState == SpawnerState.Active)
        {
            _wizardAnim.SetTrigger("Atack");
            _fireballCount.AddCount();
            Instantiate(_fireballPrefab, new Vector2(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, this.gameObject.transform.position.y), Quaternion.identity);
            _spawnerState = SpawnerState.Waiting;
            _cooldownSpawnerTime = 0f;
        }

        _cooldownSpawnerTime += Time.deltaTime;

        if (_cooldownSpawnerTime >= _spawnTimeLapse && _spawnerState == SpawnerState.Waiting)
        {
            _spawnerState = SpawnerState.Active;
            _wizardAnim.SetTrigger("Charged");
        }

    }
}
