using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovementAuto : MonoBehaviour
{
    private Facing _facing;
    Rigidbody2D _rb;
    RaycastHit2D _hit;
    Animator _anim;
    float _direction = 1;
    Collider2D _collider;
    float angle = 15f;


    // Start is called before the first frame update
    void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
        _facing = Facing.Right;
        _anim = GetComponent<Animator>();
        _collider = GetComponent<Collider2D>();
    }

    // Update is called once per frame
    void Update()
    {
        _anim.SetFloat("VerticalSpeed", _rb.velocity.y);
    }

    private void FixedUpdate()
    {
        Move();
        DetectEnemy();
        DetectGround();
        //DetectEnemy();
    }

    private void DetectEnemy()
    {
        _hit = Physics2D.Raycast(new Vector2(_collider.bounds.center.x, _collider.bounds.max.y), Vector2.right * _direction, 1f);
        Debug.DrawRay(new Vector2(_collider.bounds.center.x, _collider.bounds.max.y), Vector2.right * _direction,Color.green, 1f);
        if (_hit.collider?.gameObject.tag == "Fireball")
        {
            Debug.Log("Flipear");
            FlipCharacterWhereIsFacing();
        }

    }

    private void DetectGround()
    {
        switch (_facing)
        {
            case Facing.Left:
                _hit = Physics2D.Raycast(new Vector2(_collider.bounds.min.x, _collider.bounds.min.y), Vector2.down);
                break;
            case Facing.Right:
                _hit = Physics2D.Raycast(new Vector2(_collider.bounds.max.x, _collider.bounds.min.y), Vector2.down);
                break;
            default:
                break;
        }
        
        // && hit.collider.tag == "Ground"
        if (_hit.transform == null)
        {
            Debug.Log("Flipear");
            FlipCharacterWhereIsFacing();
        }

        Debug.DrawRay(new Vector2(_collider.bounds.max.x, _collider.bounds.min.y), Vector2.down);
    }

    public void Move()
    {
        _rb.velocity = gameObject.GetComponent<PlayerData>().ActualSpeed() * 250 * new Vector2(_direction * Time.deltaTime, 0);
        _anim.SetFloat("HorizontalSpeed", Mathf.Abs(_rb.velocity.x));
    }

    private void FlipCharacterWhereIsFacing()
    {
        _direction *= -1;
        
        switch (_facing)
        {
            case Facing.Left:
                _facing = Facing.Right;
                break;
            case Facing.Right:
                _facing = Facing.Left;
                break;
            default:
                break;
        }
        

        if (_facing == Facing.Right) transform.rotation = Quaternion.Euler(0, 0, 0);
        else transform.rotation = Quaternion.Euler(0, 180f, 0);
    }
}
