using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor.Animations;
using UnityEngine;

public enum Facing { Left, Right };

public class PlayerMovementController : MonoBehaviour
{

    private Facing _facing;
    Rigidbody2D _rb;
    RaycastHit2D hit;
    Animator _anim;
    BoxCollider2D _collider2d;

    // Start is called before the first frame update
    void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
        _facing = Facing.Right;
        _anim = GetComponent<Animator>();
        _collider2d = GetComponent<BoxCollider2D>();

    }


    private void Update()
    {
        if (hit.collider?.tag == "Ground")
            Jump();

        _anim.SetFloat("VerticalSpeed", _rb.velocity.y);
        FlipCharacterWhereIsFacing();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        //Debug.Log(grounded);
        //Physics2D.Raycast(transform.position, Vector2.down, out hit, (gameObject.GetComponent<BoxCollider2D>().size.y / 2) + 0.13f)
        hit = Physics2D.Raycast(new Vector2(_collider2d.bounds.center.x, _collider2d.bounds.min.y), Vector2.down, 0.13f);
        Debug.DrawRay(new Vector2(_collider2d.bounds.center.x, _collider2d.bounds.min.y), Vector2.down, Color.red, 0.13f);

        if (hit.collider?.tag == "Ground")
        {
            Move();
            _anim.SetBool("Grounded", true);
        }
    }


    void Jump()
    {
        if (Input.GetButtonDown("Jump"))
        {
            _rb.AddForce(new Vector2(0, 1000f));
            _anim.SetBool("Grounded", false);
            _anim.SetTrigger("Jump");
        }
    }

    public void Move()
    { 
       _rb.velocity = gameObject.GetComponent<PlayerData>().ActualSpeed() * 250 * new Vector2(Input.GetAxis("Horizontal") * Time.deltaTime, 0);
       _anim.SetFloat("HorizontalSpeed", Mathf.Abs(_rb.velocity.x)); 
    }

    
    private void FlipCharacterWhereIsFacing()
    {
        if (_rb.velocity.x > 0) _facing = Facing.Right;
        else if (_rb.velocity.x < 0) _facing = Facing.Left;

        if(_facing == Facing.Right) transform.rotation = Quaternion.Euler(0, 0, 0);
        else transform.rotation = Quaternion.Euler(0, 180f, 0);   
    }

}
