using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerData : MonoBehaviour
{

    //nombre
    //tipo de presonaje
    //altura
    //velocidad
    //distacia a recorrer



    public enum playerType
    {
        soldier,
        ninja,
        freeMan,
        terminator,
        langa
    }

    
    public string PlayerName;
    public playerType PlayerType;
    public float Height;
    public float Speed;
    public float Weight;


    public float ActualSpeed()
    {
        return (Speed / Weight);
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }
}
