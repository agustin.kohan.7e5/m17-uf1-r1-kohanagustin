using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatePlayer : MonoBehaviour
{

    private SpriteRenderer _sr;
    private int _frames;
    [SerializeField]
    private int _frameRate = 0;

    // Start is called before the first frame update
    void Start()
    {
        _sr = GetComponent<SpriteRenderer>();
        _frames = 0;
    }

    // Update is called once per frame
    void Update()
    {
        Animate();
    }

    [SerializeField]
    private Sprite[] _sprites;
    private int _contSprite;

    void Animate()
    {
        if (_frames >= _frameRate * Time.deltaTime)
        {
            _frames = 0;
            _sr.sprite = _sprites[_contSprite];
            _contSprite = _contSprite < _sprites.Length - 1 ? _contSprite + 1 : 0;
        }
        _frames++;
    }
}
