using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Patrol : MonoBehaviour
{ 
    private Facing _facing;
    Rigidbody2D _rb;
    RaycastHit2D hit;
    bool raycastHit;
    Animator _anim;
    float _direction;
    Collider2D _collider;


    // Start is called before the first frame update
    void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
        _facing = Facing.Right;
        _anim = GetComponent<Animator>();
        _collider = GetComponent<Collider2D>();
    }

    // Update is called once per frame
    void Update()
    {
        _anim.SetFloat("VerticalSpeed", _rb.velocity.y);
    }

    private void FixedUpdate()
    {
        Move();
        DetectGround();
        DetectEnemy();
    }

    private void DetectEnemy()
    {
        
    }

    private void DetectGround()
    {
        if (Physics2D.Raycast(transform.position, Quaternion.Euler(0, 0, 15) * Vector2.down))
        {
            hit = Physics2D.Raycast(transform.position, Quaternion.Euler(0, 0, 15) * Vector2.down);
            raycastHit = true;
            // && hit.collider.tag == "Ground"
            if (hit.transform == null)
            {
                FlipCharacterWhereIsFacing();
            }
        }
        else raycastHit = false;

        Debug.DrawRay(transform.position, Quaternion.Euler(0, 0, 15) * Vector2.down);
    }

    public void Move()
    {
        _rb.velocity = gameObject.GetComponent<PlayerData>().ActualSpeed() * 250 * new Vector2(_direction * Time.deltaTime, 0);
        _anim.SetFloat("HorizontalSpeed", Mathf.Abs(_rb.velocity.x));
    }

    private void FlipCharacterWhereIsFacing()
    {
        switch (_facing)
        {
            case Facing.Left:
                _facing = Facing.Right;
                _direction = 1;
                break;
            case Facing.Right:
                _facing = Facing.Left;
                _direction = -1;
                break;
            default:
                break;
        }

        if (_facing == Facing.Right) transform.rotation = Quaternion.Euler(0, 0, 0);
        else transform.rotation = Quaternion.Euler(0, 180f, 0);
    }
}
