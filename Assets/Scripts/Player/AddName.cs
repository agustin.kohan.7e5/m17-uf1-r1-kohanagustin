using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AddName : MonoBehaviour
{

    private GameObject PlayerData;
    [SerializeField] private Text _playername = null;

    // Start is called before the first frame update
    void Start()
    {
        PlayerData = GameObject.Find("PlayerName");
        LoadName();
    }

    void LoadName()
    {
        _playername.text = PlayerData.GetComponent<PlayerName>().NamePlayer;
    }
}
