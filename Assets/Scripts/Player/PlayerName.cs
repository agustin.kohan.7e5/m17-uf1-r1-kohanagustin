using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerName : MonoBehaviour
{

    public string NamePlayer;


    [SerializeField] private InputField _playerNameField = null;

    public void SavePlayerName()
    {
        gameObject.GetComponent<PlayerName>().NamePlayer = _playerNameField.text;
    }

    // Start is called before the first frame update

    private static PlayerName instance = null;

    //Game Instance Singleton

    public static PlayerName Instance { get { return instance; } }
    void Start()
    {
        // if the singleton hasn't been initialized yet
        if (instance != null && instance != this) { Destroy(this.gameObject); }

        instance = this;
        DontDestroyOnLoad(gameObject);
    }
}
